# service oriented architecture
> <cite> "Service-Oriented Architecture (SOA) is an architectural approach in which applications make use of services available in the network. In this architecture, services are provided to form applications, through a communication call over the internet." </cite>

***The Structure and makeup to complex engineered system is fundamentally different from Traditional engineered system.***

#### Which are (*Traditional engineered system*)

- Homogenious.
- well bounded and Relativily static.
<img src="Trad2.jpg" alt="drawing" width="300"/>

#### Our Complex engineered System are :

- Heterogeneous.
- Unbounded.
- Dyanamic
- and composed of autonomous element.
 


    <img src="complx.jpg" alt="drawing" width="300"/>
   

  Modeling and designing these new complex engineered systems requires intern and alternative Paradigm in systems architecture, our new architecture will need to be able to deal with the key features to complex engineered systems that we memtioned above.
- firstly it will need to be focused on Services over the properties of components.
- it will also need to be focused upon interpretability  and cross-platform functionality to deal with a high level of diversity between components So is to deal with the autonomy of the components.
- it will need to be flexible distributed and what we call Loosely coupled.
- lastly, It will also need to employ a high level of abstraction to be able to deal with the overwhelming complexity of these systems.



   <img src="image.jpg" alt="image" width="750"/>

   **service oriented architecture or soa** for short is an approach to distributed systems architecture than employees Loosely coupled Services standard interfaces and protocols to deliver seamless cross platform integration. It is used to integrate widely Divergent components by providing them with a common interface and a set of protocols for them to communicate through what is called a service bus.


   ***Let's take an example to understand the concept of SOA.***
   Imagine, I want to build a new web application that allows people to pay their parking tickets online.    well I could spend years developing a sub system that functions as a street map and then another subsystem for dealing with the payments and yet another for login user authentication and so on..
 
    or I could simply Avail of Google's Map service a payment Gateway service from PayPal and a user login service from Facebook or gmail or so on.., ***now my job then would be to integrate these diverse services by creating some common process that guides the user through the use of these different services to deliver the desired functionality, Thus instead of building a system that was based around all my different internal components are now  well bounded  by piece of software.***

    <img src="SOA.jpg" alt="image" width="750"/>

    and new application would instead be built with an architecture that is oriented around Services a ***service-oriented architecture.***
    <br/>  

    There are two major roles within Service-oriented Architecture: 

    - **Service provider**: The service provider is the maintainer of the service and the organization that makes available one or more services for others to use. To advertise services, the provider can publish them in a registry, together with a service contract that specifies the nature of the service, how to use it, the requirements for the service, and the fees charged.
    - **Service consumer**: The service consumer can locate the service metadata in the registry and develop the required client components to bind and use the service.
    <br/>  

    <img src="soa1.png" alt="image" width="750"/>
    <br/>   <br/>  

     
    ***Components of SOA.***

    <img src="components.png" alt="image" width="750"/>
    <br/>  <br/>  


    **Guiding Principles of SOA:**

    - **Standardized service contract:** Specified through one or more service description documents.
    - **Abstraction:** A service is completely defined by service contracts and description documents. They hide their logic, which is encapsulated within their implementation.
    - **Reusability:** Designed as components, services can be reused more effectively, thus reducing development time and the associated costs.
    - **utonomy**: Services have control over the logic they encapsulate and, from a service consumer point of view, there is no need to know about their implementation.
    - **Discoverability**: Services are defined by description documents that constitute supplemental metadata through which they can be effectively discovered. Service discovery provides an effective means for utilizing third-party resources.
    - **Composability:** Using services as building blocks, sophisticated and complex operations can be implemented. Service orchestration and choreography provide solid support for composing services and achieving business goals.
     <br/>      
     
    ***Advantages of SOA***: 
  
     
    - **Service reusability:** In SOA, applications are made from existing services. Thus, services can be reused to make other applications.
    - **Easy maintenance:** As services are independent of each other they can be updated and modified easily without affecting other services.
    - **Platform independent:** SOA allows making a complex application by combining services picked from different sources, independent of the       platform.
    - **Availability:** SOA facilities are easily available to anyone on request.
    - **Reliability:** SOA applications are more reliable because it is easy to debug small services rather than huge codes
    - **Scalability: Services can run on different servers within an environmen**t, this increases scalability
                             
     <br/>      
                         

    ***Disadvantages of SOA:*** 
  

    - **High overhead** 
    - **High investment** 
    - **Complex service management** 
<br/>
    Reference:
    - [Wikipedia.](https://en.wikipedia.org/wiki/Service-oriented_architecture)
